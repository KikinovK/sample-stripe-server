# sample-stripe-server
creates a server to test the connection of the payment system

docs: https://docs.stripe.com/payments/quickstart

## Getting Started

This is an example of how you can run a project locally. Follow these simple steps as an example.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/KikinovK/sample-stripe-server.git
   ```
2. Install NPM packages
   ```sh
   npm install
   ```

<!-- USAGE EXAMPLES -->

## Usage

### Running the Application

To run the application, use the following command:

```
   npm run start
```
